//
//  GalleryViewController.swift
//  ExamenGigigo
//
//  Created by Efrén Aguilar on 8/8/15.
//  Copyright (c) 2015 Efrén Aguilar. All rights reserved.
//

import UIKit
import AVFoundation
import MediaPlayer

class GalleryViewController : UIViewController{
    var imagesScrollView: UIScrollView!
    var songsScrollView: UIScrollView!
    var videosScrollView: UIScrollView!
    var audioPlayer: AVAudioPlayer?
    
    
    
    func reproduceAudio(sender: UITapGestureRecognizer){
        
        var songText = (sender.view!.subviews[1] as! UILabel).text
    
        if let path = NSBundle.mainBundle().pathForResource(songText?.stringByDeletingPathExtension, ofType: "mp3") {
            AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback, error: nil)
            AVAudioSession.sharedInstance().setActive(true, error: nil)
            
            var error:NSError?
            audioPlayer = AVAudioPlayer(contentsOfURL: NSURL(fileURLWithPath: path), error: &error)
            audioPlayer!.prepareToPlay()
            audioPlayer!.play()
        }
    }
    
    func reproduceVideo(sender: UITapGestureRecognizer){
        var videoText = (sender.view!.subviews[1] as! UILabel).text
        
        if let
            path = NSBundle.mainBundle().pathForResource(videoText?.stringByDeletingPathExtension, ofType:"m4v"),
            url = NSURL(fileURLWithPath: path),
            moviePlayer = MPMoviePlayerViewController(contentURL: url){

                presentMoviePlayerViewControllerAnimated(moviePlayer)
        }
    }
    
    func rotated(){
        imagesScrollView.frame = CGRectMake(0, 90, self.view.frame.size.width, 200.0)
        songsScrollView.frame = CGRectMake(0, 480, self.view.frame.size.width, 100.0)
        videosScrollView.frame = CGRectMake(0, 340, self.view.frame.size.width, 100.0)
    }
    
    // MARK: UIViewController
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "rotated", name: UIDeviceOrientationDidChangeNotification, object: nil)
        
        let imagesTitleLabel = UILabel(frame: CGRectMake(10, 50, self.view.frame.size.width, 30))
        imagesTitleLabel.text = NSLocalizedString("images_title", comment: "Images title label")
        self.view.addSubview(imagesTitleLabel)
        
        // Images Scroll View
        imagesScrollView = UIScrollView(frame: CGRectMake(0, 90, self.view.frame.size.width, 200.0))
        imagesScrollView.showsHorizontalScrollIndicator = true
        imagesScrollView.scrollEnabled = true
        imagesScrollView.backgroundColor = UIColor.groupTableViewBackgroundColor()
        imagesScrollView.indicatorStyle = UIScrollViewIndicatorStyle.Black
        
        self.view.addSubview(imagesScrollView)
        
        
        
        let videosTitleLabel = UILabel(frame: CGRectMake(10, 300, self.view.frame.size.width, 30))
        videosTitleLabel.text = NSLocalizedString("videos_title", comment: "Videos title label")
        self.view.addSubview(videosTitleLabel)
        
        // Videos Scroll View
        videosScrollView = UIScrollView(frame: CGRectMake(0, 340, self.view.frame.size.width, 100.0))
        videosScrollView.showsHorizontalScrollIndicator = true
        videosScrollView.scrollEnabled = true
        videosScrollView.backgroundColor = UIColor.groupTableViewBackgroundColor()
        videosScrollView.indicatorStyle = UIScrollViewIndicatorStyle.Black
        
        self.view.addSubview(videosScrollView)
        
        
        
        let songsTitleLabel = UILabel(frame: CGRectMake(10, 440, self.view.frame.size.width, 30))
        songsTitleLabel.text = NSLocalizedString("audio_title", comment: "Songs title label")
        self.view.addSubview(songsTitleLabel)
        
        // Songs Scroll View
        songsScrollView = UIScrollView(frame: CGRectMake(0, 480, self.view.frame.size.width, 100.0))
        songsScrollView.showsHorizontalScrollIndicator = true
        songsScrollView.scrollEnabled = true
        songsScrollView.backgroundColor = UIColor.groupTableViewBackgroundColor()
        songsScrollView.indicatorStyle = UIScrollViewIndicatorStyle.Black
        
        self.view.addSubview(songsScrollView)
    }
    
    override func viewWillAppear(animated: Bool) {
        var imagesArray: Array<String>!
        var videosArray: Array<String>!
        var songsArray: Array<String>!
        let path = NSBundle.mainBundle().pathForResource("galleryResources", ofType: "plist")
        let dict = NSDictionary(contentsOfFile: path!)
        
        imagesArray = dict?.valueForKey("images") as! Array<String>
        songsArray = dict?.valueForKey("audios") as! Array<String>
        videosArray = dict?.valueForKey("videos") as! Array<String>
        
        
        var x: CGFloat = 10.0
        
        for s in imagesArray{
            let imageView = UIImageView(image: UIImage(named: s))
            
            imageView.frame = CGRectMake(x, 10, 180, 180)
            imageView.contentMode = UIViewContentMode.ScaleAspectFit
            imagesScrollView.addSubview(imageView)
            imagesScrollView.contentSize = CGSize(width: (x + 180), height: 180)
            x = x + 195.0
            
            imagesScrollView.frame = CGRectMake(0, 90, self.view.frame.size.width, 200.0)
            imagesScrollView.contentSize = CGSize(width: x , height: 180)
        }
        
        x = 10.0
        for s in songsArray{
            let view = UIView(frame: CGRectMake(x, 10, 130, 80))
            
            let songImage = UIImageView(image: UIImage(named: "musicNote.png"))
            songImage.frame = CGRectMake(0, 0, 50, 50)
            songImage.center = CGPoint(x: 65, y: 40)
            view.addSubview(songImage)
            
            let songTitle = UILabel(frame: CGRectMake(0, 60, 130, 40))
            songTitle.font = UIFont(name: "System", size: 8)
            songTitle.textAlignment = NSTextAlignment.Center
            songTitle.text = "\(s).mp3"
            view.addSubview(songTitle)
            
            var singleTabImageView = UITapGestureRecognizer(target: self, action: Selector("reproduceAudio:"))
            singleTabImageView.numberOfTapsRequired = 1
            view.addGestureRecognizer(singleTabImageView)
            songsScrollView.addSubview(view)
            
            x = x + 140
            songsScrollView.frame = CGRectMake(0, 480, self.view.frame.size.width, 100.0)
            songsScrollView.contentSize = CGSize(width: x, height: 80)
        }
        
        x = 10.0
        for s in videosArray{
            let view = UIView(frame: CGRectMake(x, 10, 130, 80))
            
            let songImage = UIImageView(image: UIImage(named: "musicNote.png"))
            songImage.frame = CGRectMake(0, 0, 50, 50)
            songImage.center = CGPoint(x: 65, y: 40)
            view.addSubview(songImage)
            
            let songTitle = UILabel(frame: CGRectMake(0, 60, 130, 40))
            songTitle.font = UIFont(name: "System", size: 8)
            songTitle.textAlignment = NSTextAlignment.Center
            songTitle.text = "\(s).m4v"
            view.addSubview(songTitle)
            
            var singleTabImageView = UITapGestureRecognizer(target: self, action: Selector("reproduceVideo:"))
            singleTabImageView.numberOfTapsRequired = 1
            view.addGestureRecognizer(singleTabImageView)
            videosScrollView.addSubview(view)
            
            x = x + 140
            videosScrollView.frame = CGRectMake(0, 340, self.view.frame.size.width, 100.0)
            videosScrollView.contentSize = CGSize(width: x, height: 80)
        }
    }
 
    override func viewWillDisappear(animated: Bool) {
        if audioPlayer != nil {
            audioPlayer?.stop()
        }
    }
    
}
