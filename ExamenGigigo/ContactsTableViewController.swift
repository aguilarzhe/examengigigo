//
//  ContactosTableViewController.swift
//  ExamenGigigo
//
//  Created by Efrén Aguilar on 8/6/15.
//  Copyright (c) 2015 Efrén Aguilar. All rights reserved.
//

import UIKit
import CoreData

class ContactsTableViewController : UITableViewController, UITableViewDataSource{
    @IBOutlet var contactosTableView: UITableView!
    var contacts = [NSManagedObject]()
    var selectedContact : NSManagedObject!
    var managedContext : NSManagedObjectContext!
    
    
    func addContact(){
        performSegueWithIdentifier("addContactSegue", sender: self)
    }
    
    // MARK: UIViewController
    override func viewDidLoad() {
        super.viewDidLoad()
        
        contactosTableView.dataSource = self
        self.title =  NSLocalizedString("contacts", comment: "Contact title")

        navigationItem.rightBarButtonItem = self.editButtonItem()
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Add, target: self, action: Selector("addContact"))
        
    }
    
    override func viewWillAppear(animated: Bool) {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        managedContext = appDelegate.managedObjectContext!
        let fetchRequest = NSFetchRequest(entityName:"Contact")
        var error: NSError?
        
        let fetchedResults = managedContext.executeFetchRequest(fetchRequest, error: &error) as? [NSManagedObject]
        
        if let results = fetchedResults {
            contacts = results
            self.tableView.reloadData()
        } else {
            println("Could not fetch \(error), \(error!.userInfo)")
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
    
    // MARK: UITableViewDataSource
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contacts.count;
    }
    
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        switch editingStyle {
        case .Delete:
            let appDelegate : AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            let context:NSManagedObjectContext = appDelegate.managedObjectContext!
            context.deleteObject(contacts[indexPath.row] as NSManagedObject)
            contacts.removeAtIndex(indexPath.row)
            context.save(nil)
            
            self.tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        default:
            return
            
        }
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier("contactCell") as? UITableViewCell
        
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: "contactCell")
        }
        
        let contact = contacts[indexPath.row]
        cell?.textLabel?.text = contact.valueForKey("name") as? String
        cell?.detailTextLabel?.text = contact.valueForKey("phone") as? String
        
        return cell!
    }
    
    override func tableView(tableView: UITableView, willSelectRowAtIndexPath indexPath: NSIndexPath) -> NSIndexPath? {
        
        selectedContact = contacts[indexPath.row]
        
        return indexPath
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showContact"{
            let contactViewController = segue.destinationViewController as! ContactViewController
            
            contactViewController.contact = selectedContact
            contactViewController.managedContext = managedContext
        }
    }
}
