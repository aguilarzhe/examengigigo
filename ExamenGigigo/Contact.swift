//
//  Contact.swift
//  ExamenGigigo
//
//  Created by Efrén Aguilar on 8/7/15.
//  Copyright (c) 2015 Efrén Aguilar. All rights reserved.
//

import UIKit

class Contact{
    var name : String!
    var lastname : String!
    var phone : String!
    var address : String!
    var photo : UIImage? = UIImage()
}