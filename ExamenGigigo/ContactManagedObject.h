//
//  ContactManagedObject.h
//  ExamenGigigo
//
//  Created by Efrén Aguilar on 8/7/15.
//  Copyright (c) 2015 Efrén Aguilar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface ContactManagedObject : NSManagedObject

@property (nonatomic, retain) NSString * lastname;
@property (nonatomic, retain) NSString * address;
@property (nonatomic, retain) NSData * photo;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * phone;

@end
