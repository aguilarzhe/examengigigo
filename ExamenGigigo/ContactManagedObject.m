//
//  ContactManagedObject.m
//  ExamenGigigo
//
//  Created by Efrén Aguilar on 8/7/15.
//  Copyright (c) 2015 Efrén Aguilar. All rights reserved.
//

#import "ContactManagedObject.h"


@implementation ContactManagedObject

@dynamic lastname;
@dynamic address;
@dynamic photo;
@dynamic name;
@dynamic phone;

@end
