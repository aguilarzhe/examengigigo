//
//  ViewController.swift
//  ExamenGigigo
//
//  Created by Efrén Aguilar on 8/6/15.
//  Copyright (c) 2015 Efrén Aguilar. All rights reserved.
//

import UIKit
import MapKit

class MapViewController: UIViewController, MKMapViewDelegate {
    var mapView : MKMapView!
    var startLocation : CLLocationCoordinate2D!
    var endLocation : CLLocationCoordinate2D!
    
    func rotated(){
        mapView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)
    }
    
    private func setFirstAnnotation(location loc: CLLocationCoordinate2D, annotationTitle title: String){
        let annotation = MKPointAnnotation()
        annotation.coordinate = loc
        annotation.title = title
        annotation.subtitle = "Punto de inicio"

        mapView.addAnnotation(annotation)
    }
    
    private func setAnnotation(location loc: CLLocationCoordinate2D, annotationTitle title: String){
        let annotation = MKPointAnnotation()
        annotation.coordinate = loc
        annotation.title = title
        mapView.addAnnotation(annotation)
    
    }
    
    private func consumeWebService(){
        let baseUrl: String = "http://maps.googleapis.com"
        let uri: String = "/maps/api/directions/json"
        var request : NSMutableURLRequest = NSMutableURLRequest()
       
        let urlComponents = NSURLComponents(string: baseUrl)!
        urlComponents.path = uri
        urlComponents.query = "origin=Chicago,IL&destination=Los+Angeles,CA&waypoints=Joplin,MO|Oklahoma+City,OK&sensor=false"
        request.URL = urlComponents.URL
        
        request.HTTPMethod = "GET"
        
        var response : AutoreleasingUnsafeMutablePointer<NSURLResponse?> = AutoreleasingUnsafeMutablePointer<NSURLResponse?>()
        var data = NSURLConnection.sendSynchronousRequest(request, returningResponse: response, error: nil)
        let jsonResult: NSDictionary! = NSJSONSerialization.JSONObjectWithData(data!, options:NSJSONReadingOptions.MutableContainers, error: nil) as? NSDictionary
            
            if (jsonResult != nil) {
                var dictionaryStartLocation = (((((jsonResult["routes"] as! NSArray)[0] as! NSDictionary)["bounds"]  as! NSDictionary)["northeast"]) as! NSDictionary)
                startLocation = CLLocationCoordinate2D(latitude: dictionaryStartLocation["lat"] as! CLLocationDegrees, longitude: dictionaryStartLocation["lng"] as! CLLocationDegrees)
                setFirstAnnotation(location: startLocation, annotationTitle: "Chicago")
                
                var legs = (((jsonResult["routes"] as! NSArray)[0] as! NSDictionary)["legs"]  as! NSArray)
                
                for l in legs{
                    let dic = l as! NSDictionary
                    let location = l["end_location"] as! NSDictionary
                    var coord = CLLocationCoordinate2D(latitude: location["lat"] as! CLLocationDegrees, longitude: location["lng"] as! CLLocationDegrees)
                    
                    setAnnotation(location: coord, annotationTitle: l["end_address"] as! String)
                }

            }
        
    }
    
    // MARK: MKMapViewDelegate
    func mapView(mapView: MKMapView!, rendererForOverlay overlay: MKOverlay!) -> MKOverlayRenderer! {
        if (overlay is MKPolyline) {
            var pr = MKPolylineRenderer(overlay: overlay);
            pr.strokeColor = UIColor.blueColor().colorWithAlphaComponent(0.5);
            pr.lineWidth = 5;
            return pr;
        }
        
        return nil
    }
    
    // MARK: ViewController
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "rotated", name: UIDeviceOrientationDidChangeNotification, object: nil)
        
        mapView = MKMapView()
        self.view.addSubview(mapView)

        consumeWebService()
        
        mapView.delegate = self
    }
    
    override func viewWillAppear(animated: Bool) {
        mapView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }


}

