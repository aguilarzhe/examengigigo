//
//  ContactsViewController.swift
//  ExamenGigigo
//
//  Created by Efrén Aguilar on 8/6/15.
//  Copyright (c) 2015 Efrén Aguilar. All rights reserved.
//

import UIKit
import CoreData
import MobileCoreServices

class ContactViewController: UIViewController, UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var lastnameTextField: UITextField!
    @IBOutlet weak var addressTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var photoImageView: UIImageView!
    var contact : NSManagedObject!
    var managedContext : NSManagedObjectContext!
    
    
    func updateContactImage(){
        var actionSheet = UIActionSheet(title: nil, delegate: self, cancelButtonTitle: "Cancel", destructiveButtonTitle: nil, otherButtonTitles: "Choose Photo", "Take photo", "Clear image")
        actionSheet.showInView(self.view)

    }
    
    func saveContact(){
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext!
        let entity =  NSEntityDescription.entityForName("Contact", inManagedObjectContext: managedContext)
        
        let person = NSManagedObject(entity: entity!,
            insertIntoManagedObjectContext:managedContext)
        
        person.setValue(nameTextField.text, forKey: "name")
        person.setValue(lastnameTextField.text, forKey: "lastname")
        person.setValue(addressTextField.text, forKey: "address")
        person.setValue(phoneTextField.text, forKey: "phone")
        person.setValue(UIImagePNGRepresentation(photoImageView.image), forKey: "photo")
        
        var error: NSError?
        if !managedContext.save(&error) {
            println("Could not save \(error), \(error?.userInfo)")
        }
        
        self.navigationController!.popViewControllerAnimated(true)
    }
    
    func updateContact(){
        var saveError : NSError? = nil
        let contactManagedObject = contact as! ContactManagedObject
        contactManagedObject.name = nameTextField.text
        contactManagedObject.lastname = lastnameTextField.text
        contactManagedObject.phone = phoneTextField.text
        contactManagedObject.address = addressTextField.text
        contactManagedObject.photo = UIImagePNGRepresentation(photoImageView.image)
        
        if !managedContext!.save(&saveError) {
            println("Could not update record")
        }
        self.navigationController!.popViewControllerAnimated(true)
    }
    
    // MARK: UIViewController
    override func viewDidLoad() {
        super.viewDidLoad()
        
        nameTextField.placeholder = NSLocalizedString("name_placeholder",comment: "Contact's name")
        lastnameTextField.placeholder = NSLocalizedString("lastname_placeholder", comment: "Contact's lastname")
        addressTextField.placeholder = NSLocalizedString("address_placeholder", comment: "Contact's lastname")
        phoneTextField.placeholder = NSLocalizedString("phone_placeholder", comment: "Contact's lastname")

        // Set imageView configuration
        var singleTabImageView = UITapGestureRecognizer(target: self, action: Selector("updateContactImage"))
        singleTabImageView.numberOfTapsRequired = 1
        photoImageView.layer.cornerRadius = 55
        photoImageView.layer.masksToBounds = true
        photoImageView.userInteractionEnabled = true
        photoImageView.addGestureRecognizer(singleTabImageView)
        photoImageView.image = UIImage(named: "icon-2.png")
        
    }
    

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        if  contact == nil {
            
            
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Done, target: self, action: Selector("saveContact"))
        }else{
            
            nameTextField.text = contact.valueForKey("name") as? String
            lastnameTextField.text = contact.valueForKey("lastname") as? String
            phoneTextField.text = contact.valueForKey("phone") as? String
            addressTextField.text = contact.valueForKey("address") as? String
            photoImageView.image = UIImage(data: contact.valueForKey("photo") as! NSData)
            
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Done, target: self, action: Selector("updateContact"))
        }
    }
    
    
    // MARK: ActionSheetDelegate
    func actionSheet(actionSheet: UIActionSheet, clickedButtonAtIndex buttonIndex: Int) {
        switch(buttonIndex){
        case 1:
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary)
            {
                var imag = UIImagePickerController()
                imag.delegate = self
                imag.sourceType = UIImagePickerControllerSourceType.PhotoLibrary;
                imag.mediaTypes = [kUTTypeImage]
                imag.allowsEditing = false
                
                self.presentViewController(imag, animated: true, completion: nil)
            }
            break;
        case 2:
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera)
            {
                var imag = UIImagePickerController()
                imag.delegate = self
                imag.sourceType = UIImagePickerControllerSourceType.Camera;
                imag.mediaTypes = [kUTTypeImage]
                imag.allowsEditing = false
                
                self.presentViewController(imag, animated: true, completion: nil)
            }
            break;
        default:
            break;
        }
    }
    
    // MARK: UIImagePickerControllerDelegate
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage!, editingInfo: [NSObject : AnyObject]!) {
        picker.dismissViewControllerAnimated(true, completion: { () -> Void in
            
        })
        photoImageView.image = image
    }
}
